## MiniX05

[Click to view my **miniX05**](https://tilla-ap.gitlab.io/aesthetic-programming/miniX05/index.html)

[Click to view my **code**](https://gitlab.com/tilla-ap/aesthetic-programming/-/blob/main/miniX05/sketch.js)

![](miniX05Video.mov)

**What are the rules in your generative program? Describe how your program
performs over time? How do the rules produce emergent behavior?**

Here are the rules of my generative program:
1. A circle in a random color appears at a random spot every 1 to 10 seconds
2. If two circles collides, they emerge to one bigger circle by increasing its radius

The program continues to run and change without any user interaction, based on the two rules listed above. The rules produce emergent behavoiur by creating unpredictable patterns with the circles random movement, and change in direction when reaching the edges of the page. The collision function also contributes to circles having different sizes when they emerge and grow, and creates a hierarchical structure in circle sizes where the bigger circles eats the smaller circles. 

**What role do rules and processes have in your work?**

Rules and processes play a fundamental part in defining the behavior of the circles within the program. These rules determine how the circles interact with each other, move, grow, and change over time. 

**Draw upon the assigned reading, how does this MiniX help you to understand the
idea of “auto-generator” (e.g. levels of control, autonomy, love and care via
rules)? Do you have any further thoughts on the theme of this chapter?**

The rules of the program is set up by the programmer, and I also had control over parameters of the system, such as circle size, speed, and collision behavior. However, once the program is running, the emergent behavior of the circles is not directly controlled by the programmer, but is instead influenced by the interactions between the circles based on the defined rules. This illustrates a level of control where the programmer sets the framework and conditions for behavior, but allows for emergent outcomes within those constraints. The MiniX therefore helped me illustrate how randomness create unpredictable patterns, and in a way creates a life of its own.

**References**

https://www.w3schools.com/jsref/jsref_for.asp

https://www.w3schools.com/jsref/jsref_push.asp

https://p5js.org/reference/#/p5/splice

Soon Winnie & Cox Geoff (2020), "Data Capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press (Chapter 5)