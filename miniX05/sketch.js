let circles = [];  // Array of circles
let nextCircleTime = 0;  // The first circle appears immediatly when the program starts running
let interval = 1000; // Minimum interval set to 1 second

function setup() {
  createCanvas(windowWidth, windowHeight);
}

function draw() {
  background(255);  // White background

  // For-loop to move and display the circles
  for (let i = 0; i < circles.length; i++) { //As long as i is less than the length of the array, the loop will continure to run
    circles[i].move();  // Calls the move function
    circles[i].display();  // Calls the display function
  }

  // Nested loop to check for collisions between circles
  for (let i = 0; i < circles.length; i++) {  // Loop through each circle in the array
    for (let j = i + 1; j < circles.length; j++) {  // Loop through each circle after the current one
      if (circles[i].collision(circles[j])) {  // Checks if the current circle collides with another circle by calling the collision function
        if (circles[i].radius > circles[j].radius) {
          circles[i].eat(circles[j]);  // If the current circle is bigger than the other circle, it eats the other circle
        } else {
          circles[j].eat(circles[i]);  // If the other circle is bigger than the current circle, it eats the current circle
        }
      }
    }
  }

  // Add circle at specified interval
  if (millis() > nextCircleTime) {  // Checks if it's time to add a new circle based on the time since the program started running
    addCircle();  // Calls the addCircle function
    nextCircleTime = millis() + interval; // Update next circle time
    interval = random(1000, 10000); // Randomize interval for next circle between 1 and 10 seconds
  }
}

function addCircle() {
  let randomX = random(width);  // Random x-coordinate for the new circle
  let randomY = random(height);  // Random y-coordinate for the new circle
  let randomColor = color(random(255), random(255), random(255));  // Random RGB color
  circles.push(new Circle(randomX, randomY, randomColor));  // Adds one more cirlce to the array with the randomized parameters 
}

class Circle {  // Define the initial state of new circles
  constructor(x, y, color) {  // The objects' properties
    this.x = x; // Initialize x-coordinate of the circle
    this.y = y;  // Initialize y-coordinate of the circle
    this.color = color;  // Initialize color of the circle
    this.radius = 20;  // Initialize radius of the circle
    this.speedX = random(-2, 2);  // Initialize horizontal speed of the circle with a random value between -2 and 2
    this.speedY = random(-2, 2);  // Initialize vertical speed of the circle with a random value between -2 and 2
  }

  move() {
    this.x += this.speedX;  // Updates the circle's x-coordinate by adding its horizontal speed 
    this.y += this.speedY;  // Updates the circle's y-coordinate by adding its horizontal speed 

    // Check if the circles reaches the edge of the canvas
    if (this.x < 0 || this.x > width) {   // If the circle's x-coordinate is less than 0 (left edge) or greater than canvas width (right edge)
      this.speedX *= -1;  // Reverse the horizontal speed (change direction) by multiplying by -1
    }
    if (this.y < 0 || this.y > height) {  // If the circle's y-coordinate is less than 0 (top edge) or greater than canvas height (bottom edge)
      this.speedY *= -1;  // Reverse the vertical speed (change direction) by multiplying by -1
    }
  }

  display() {  // Function to create circles with random color and coordinates, and with the specified size
    fill(this.color); 
    ellipse(this.x, this.y, this.radius);
  }

  collision(other) {
    let d = dist(this.x, this.y, other.x, other.y);  // Calculate the distance between the centers of the two circles
    return d < this.radius + other.radius;  // Check if the distance between the centers is less than the sum of the radius

  }

  eat(other) {
    // Increase radius and adjust position
    this.radius += 2;  // Increase the radius of the current circle by 2
    this.x = (this.x + other.x) / 2;  // Set the x-coordinate of the current circle to the midpoint between its original position and the position of the eaten circle
    this.y = (this.y + other.y) / 2;  // Set the y-coordinate of the current circle to the midpoint between its original position and the position of the eaten circle

    // Remove the eaten circle from the array
    circles.splice(circles.indexOf(other), 1);  // Find the index of the eaten circle in the circles array and remove it
  }
}
