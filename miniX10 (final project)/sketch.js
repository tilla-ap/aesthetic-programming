// Defining API for fetching location data
const locationAPI = 'http://ip-api.com/json/?fields=country,countryCode,city,lat,lon';
//Defining variables
let capture;
let tracker;
let button;
let scanRGB = false;
let rectCoordinates = null;
let rgbValue = null;
let age;
let crime;
let money;
let daysLeft;


function setup() {
  createCanvas(windowWidth, windowHeight);
  
   // Initialize video capture from the webcam and set its size to match the canvas
  capture = createCapture(VIDEO);
  capture.size(windowWidth, windowHeight);
  capture.hide();
  
  // Initialize the face tracking
  tracker = new clm.tracker();
  tracker.init();
  tracker.start(capture.elt);

  getLocationData(); // Fetch location data from the API
  
  // Creating a button for triggering facial scanning
  button = createButton('WHO ARE YOU?');
  button.position(650, 20); // Position the button relative to the bottom right corner
  button.mousePressed(startScan); // Call the startScan function when the button is presßsed
  button.addClass("buttonStyle") //CSS class for styling

}

function draw() {
  background(220); //clearing the canvas
  // Flip the camera feed horizontally
  translate(width, 0);  // Move the origin to the right edge of the canvas
  scale(-1, 1); // Flip the x-axis to create a mirror effect
  image(capture, 0, 0, width, height); // Draw the video feed

  // Get array of detected facial landmarks
  let positions = tracker.getCurrentPosition();

  // Check if facial landmarks are detected
  if (positions.length > 0) {
    // Draw a rectangle around the detected facial landmarks
    let minX = width;
    let minY = height;
    let maxX = 0;
    let maxY = 0;
    
    for (let i = 0; i < positions.length; i++) {
      let pos = positions[i];
      minX = min(minX, pos[0]);
      minY = min(minY, pos[1]);
      maxX = max(maxX, pos[0]);
      maxY = max(maxY, pos[1]);
    }
    
    noFill();
    stroke(0, 255, 0);
    rect(minX, minY, maxX - minX, maxY - minY);
    
    // Store rectangle coordinates
    rectCoordinates = {
      x: minX,
      y: minY,
      width: maxX - minX,
      height: maxY - minY
    };
  }

  // Check if RGB scanning is requested
  if (scanRGB && rectCoordinates) {
    // Calculate center coordinates of the rectangle
    let centerX = rectCoordinates.x + rectCoordinates.width / 2;
    let centerY = rectCoordinates.y + rectCoordinates.height / 2;
    
    // Record RGB value at the center of the rectangle
    rgbValue = capture.get(centerX, centerY);
    console.log("RGB at center:", rgbValue);
    
    // Stop scanning after one scan
    scanRGB = false;
  }
  
  // Display the text next to the square, flipping the text horizontally
  if (rgbValue) {
    fill(170, 0, 0);
    stroke(0);
    textSize(20);
    textAlign(LEFT, TOP);
    push(); // Save the current drawing state
    scale(-1, 1); // Flip horizontally
    text(`Skincolor RGB: ${rgbValue}`, -rectCoordinates.x - rectCoordinates.width + rectCoordinates.width + 10, rectCoordinates.y); // Position text next to the square
    text(`${locationInfo}`,  -rectCoordinates.x - rectCoordinates.width + rectCoordinates.width + 10, rectCoordinates.y + 20);
    text(`Age: ${age}`, -rectCoordinates.x - rectCoordinates.width + rectCoordinates.width + 10, rectCoordinates.y + 40); 
    text(`Criminal record: ${crime}`, -rectCoordinates.x - rectCoordinates.width + rectCoordinates.width + 10, rectCoordinates.y + 60); 
    text(`Net worth: ${money}`, -rectCoordinates.x - rectCoordinates.width + rectCoordinates.width + 10, rectCoordinates.y + 80); 
    text(`Days until you die: ${daysLeft}`, -rectCoordinates.x - rectCoordinates.width + rectCoordinates.width + 10, rectCoordinates.y + 100);  
    pop(); // Restore the previous drawing state
  }
}

function startScan() {
  // Start scanning RGB value at the center when the button is pressed
  scanRGB = true;

  generateRandomAge();
  generateCriminalRecord();
  generateNetWorth();
  generateDaysLeft();
}

function generateRandomAge() {
age = floor(random(17,86)); // Random age between 17 and 85
}

function generateCriminalRecord() {
const criminalRecords = ['Driving While Intoxicated', 'Theft', 'Fraud', 'Possession of Marijuana offender', 'Burglary', 'Vandalism', 'Nothing'];
crime = random(criminalRecords);
}

function generateNetWorth() {
const netWorths = ['15 000 DKK', '430 000 DKK', '1 230 000 DKK', '46 000 000 DKK', '4 000 000 DKK', '100 000 000 DKK', '200 000 DKK', '67 000 DKK', '540 000 DKK'];
money = random(netWorths);
}

function generateDaysLeft() {
daysLeft = floor(random(1,5000)); // Random number between 1 and 5000
}


function getLocationData() {
  fetch(locationAPI)
    .then(response => response.json())
    .then(data => {
      // Extract location information from the response
      locationInfo = `${data.city}, ${data.country}, ${data.lat}, ${data.lon}`;
    })
}