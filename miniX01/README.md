## MiniX01

[_Click to view my **miniX1**_](https://aesthetic-programming-tilla-ap-fd7d0797528003e34acb9cd4a7c56097.gitlab.io/miniX01/index.html)

[_Click to view my **code**_](https://gitlab.com/tilla-ap/aesthetic-programming/-/blob/main/miniX01/sketch.js?ref_type=heads)

**What have you produced?**

I wanted to explore the basic functions of p5 with this first exercise. I therefore chose to make a rat, where I was able to play wiht different shapes, colors and coordinates. As i felt more comfortable with the methods, I chose to give the rat some type of interactive function aswell, where the tail moves when you "pet" (aka click) the rat. 

**How would you describe your first independent coding experience (in relation to
thinking, reading, copying, modifying, writing code, and so on)?**

I would say this first independent coding experience with p5 made me explore and understand the basic functions of the method. It is very effective and helpful to learn programming with a "learing by doing" approach.

**How is the coding process different from, or similar to, reading and writing text?**

The coding process is different from reading and writing text, because you don´t use full and presice sentences to express what you want to happen, unlike when communicating with ChatGPT for example. It is similar to writing text though, in the sense that there is a specific structure you need to follow. 

**What does code and programming mean to you, and how does the assigned
reading help you to further reflect on these terms?**

Code and programming is definitly very challenging, but also very fun when you get the hang of it. The assigned reading help me understand more of the "whys" and not just "whats" and "hows". 

**References**

https://p5js.org/reference/#/p5/createCanvas

https://p5js.org/reference/#/p5/ellipse

https://p5js.org/reference/#/p5/background

https://p5js.org/reference/#/p5/stroke

https://p5js.org/reference/#/p5/strokeWeight

https://p5js.org/reference/#/p5/fill

https://p5js.org/reference/#/p5/line

https://p5js.org/reference/#/p5/triangle

https://p5js.org/reference/#/p5.Element/mouseClicked

Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 27-48
