let tailX1 = 320;
let tailY1 = 350;
let tailX2 = 400;
let tailY2 = 250;

function setup() {
 createCanvas(600,600);
}

function draw() {
  //Set the background to white
  background(255);

  //Draw the tail
  strokeWeight(21);
  stroke(51); //Black stroke
  line(tailX1, tailY1, tailX2, tailY2);

  strokeWeight(18);
  stroke(255, 182, 193); //Pink color
  line(tailX1, tailY1, tailX2, tailY2);

  //Draw the body
  strokeWeight(2);
  stroke(51); // Black stroke
  fill(150) // Gray color
  ellipse(230,300,200,300);

  //Draw the left ear
  strokeWeight(2);
  stroke(51); // Black stroke
  fill(150) // Gray color
  ellipse(160,70,100,100);

  strokeWeight(2);
  stroke(51); // Black stroke
  fill(255, 182, 193) // Pink color
  ellipse(160,70,60,60);

  //Draw the right ear
  strokeWeight(2);
  stroke(51); // Black stroke
  fill(150) // Gray color
  ellipse(300,70,100,100);

  strokeWeight(2);
  stroke(51); // Black stroke
  fill(255, 182, 193) // Pink color
  ellipse(300,70,60,60);

  //Draw the head
  strokeWeight(2);
  stroke(51); // Black stroke
  fill(150) // Gray color
  ellipse(230,160,200,190);

  //Draw the right eye
  fill(51) //Black color
  ellipse(200,140,15,20)

  //Draw the right eye
  fill(51) //Black color
  ellipse(260,140,15,20)

  //Draw the nose
  fill(255, 182, 193)
  triangle(230, 200, 215, 170, 245, 170);

  //Draw the whiskers
  line(200, 180, 150, 190);
  line(200, 180, 150, 180);
  line(200, 180, 150, 170);

  line(260, 180, 310, 190);
  line(260, 180, 310, 180);
  line(260, 180, 310, 170);

  //Draw the left hand
  strokeWeight(2);
  stroke(51); // Black stroke
  fill(150) // Gray color
  ellipse(190,300,40,60);

  line(185, 315, 185, 330);
  line(190, 315, 190, 330);
  line(195, 315, 195, 330);

  //Draw the right hand
  strokeWeight(2);
  stroke(51); // Black stroke
  fill(150) // Gray color
  ellipse(270,300,40,60);

  line(265, 315, 265, 330);
  line(270, 315, 270, 330);
  line(275, 315, 275, 330);

  //Draw the left foot
  strokeWeight(2);
  stroke(51); // Black stroke
  fill(150) // Gray color
  ellipse(190,440,70,40);

  //Draw the right foot
  strokeWeight(2);
  stroke(51); // Black stroke
  fill(150) // Gray color
  ellipse(275,440,70,40);
}

  //The rat moves his tail when you click on the rat
function mouseClicked() {
  tailX2 = random(370, 400);
  tailY2 = random(250, 300);
}
