let displayInstructions = true;  // Boolean variable
let tomatoX, tomatoY;
let tomatoImg, carImg;
let tomatoSize = 70;  // The diameter of the tomato
let roadWidth = 200;
let roadY = 400;  // Y-coordinate for the road
let carSpeed = 20;  // Cars moves 20 pixels per frame
let carWidth = 120; 
let carHeight = 80; 
let carGap = 250;  // The vertical distance (250 pixels) between cars on the road
let cars = [];  // An array that will holde the car objects
let tomatoSpeed = 5;  // The tomato moves 5 pixels per frame
let carsInitialized = false;  // Boolean variable
let gameEnded = false;  // Boolean variable

function preload() {
  tomatoImg = loadImage('tomato.png');
  carImg = loadImage('car.png');
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  tomatoX = width / 2;  // X-coordinate for the tomato
  tomatoY = height - 50;  // Y-coordinate for the tomato
}

function draw() {
  background(100); // Gray background

  // Draw white lines for the road
  fill(255);
  rect(-100, roadY, 200, 20);
  rect(260, roadY, 200, 20);
  rect(620, roadY, 200, 20);
  rect(980, roadY, 200, 20);
  rect(1340, roadY, 200, 20);

  // Draw the grass on both sides of the road
  fill(0, 200, 100);
  rect(0, 0, windowWidth, 170);
  rect(0, 700, windowWidth, 190);

  // Draw tomato
  image(tomatoImg, tomatoX - tomatoSize / 2, tomatoY - tomatoSize / 2, tomatoSize, tomatoSize);

  // Move tomato based on arrow key press
  if (keyIsDown(LEFT_ARROW)) {
    tomatoX -= tomatoSpeed;
  }
  if (keyIsDown(RIGHT_ARROW)) {
    tomatoX += tomatoSpeed;
  }
  if (keyIsDown(UP_ARROW)) {
    tomatoY -= tomatoSpeed;
  }
  if (keyIsDown(DOWN_ARROW)) {
    tomatoY += tomatoSpeed;
  }

  // Make sure tomato stays within the canvas 
  tomatoX = constrain(tomatoX, 0, width);
  tomatoY = constrain(tomatoY, 0, height);

  // Move cars
  if (!gameEnded) {  // Checks is the game has not ended
    for (let car of cars) {  // Iterates over each car in the cars array
      car.move();  // Calls the move method for each car
      car.display();  // Calls the display method for each car
    }

    // Check collision
    for (let car of cars) {  // Iterates over each car in the cars array
      if (car.hits(tomatoX, tomatoY)) {  // Checks if the current car has collided with the tomato
        console.log("Collision detected!");
        gameOver();  // Calls the gameOver function if the hits method is true 
      }
    }

    // Check win condition
    if (tomatoY < 120) {
      gameWon();  // Calling the gameWon function if the tomato's Y-coordinate is less than 120
    }

    if (displayInstructions) {
      showInstructions();  // If displayIntructions variable is true, it calls the showInstructions fucntion
    }
  }

  function gameOver() {
    gameEnded = true;
    fill(255);
    textSize(50);
    textAlign(CENTER, CENTER);
    text("GAME OVER!\n\nCome on ketchup.", width / 2, height / 2);
    noLoop();
  }
  
  function gameWon() {
    gameEnded = true;
    fill(255);
    textSize(50);
    textAlign(CENTER, CENTER);
    text("CONGRATULATIONS!\n\nYou crossed the road safely.", width / 2, height / 2);
    noLoop();
  }  
}

function showInstructions() {
  fill(10, 110, 45);
  rect(windowWidth / 2 - 150, windowHeight / 2 - 100, 300, 200);
  textAlign(CENTER, CENTER);
  fill(255);
  textSize(16);
  text("Game Instructions:\n\nPut your game instructions here.\n\nClick anywhere to start the game.", width / 2, height / 2);
}

// Initialize cars only once when the mouse is pressed
function initializeGame() {
  if (!carsInitialized) {
    for (let y = roadY -90; y < height - 80; y += carGap) {
      cars.push(new Car(y));
    }
    carsInitialized = true; // Set the statement to true so cars won't be initialized again
    displayInstructions = false; // Instructions disappear after cars are initialized
  }
}

class Car {
  constructor(y) {
    this.x = random(width);  // Randomly positions the car along the x-axis
    this.y = y;
    this.speed = (carSpeed);
    this.direction = random([-1, 1]); // Randomly moves the cars left(-1) or right(1)
  }

  move() {
    console.log("Game Ended:", gameEnded);
    if (!gameEnded) {
      this.x += this.speed * this.direction;
      if (this.x < -carWidth / 2) {
        this.x = width + carWidth / 2;
      } else if (this.x > width + carWidth / 2) {
        this.x = -carWidth / 2;
      }
    }
  }

  display() {
    image(carImg, this.x - carWidth / 2, this.y - carHeight / 2, carWidth, carHeight);
  }

  hits(x, y) {
    let d = dist(this.x, this.y, x, y);
    return d < (carWidth / 2 + tomatoSize / 2) / 2;
  }
}

function mousePressed() {
  if (displayInstructions) {
    initializeGame(); // Call the function to initialize the game
  }
}