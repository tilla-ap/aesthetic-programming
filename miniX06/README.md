## MiniX06

[Click to view my **miniX06**](https://tilla-ap.gitlab.io/aesthetic-programming/miniX06/index.html)

[Click to view my **code**](https://gitlab.com/tilla-ap/aesthetic-programming/-/blob/main/miniX06/sketch.js)

![](miniX06Video.mov)

**Describe how does/do your game/game objects work?**

The goal of my game is to help a tomato cross a road safely, while there are cars driving in different directions horisontally over the screen. If you get hit by a car, you lose, if you cross the road without getting hit, you win. My game was insipred by the "come on ketchup" joke we laughed off as kids. It is a very simple game with simple rules, where the program checks for collision between the tomato and the cars, and checks the position of the toamto. The tomato is controlled by the arrow tabs, and the cars are initialized by the user clicking on the screen, and they are released in a random direction every few seconds. 

**Describe how you program the objects and their related attributes, and the methods in your game.**

The two main objects in the game is the tomato (the player), and the cars (the obstacles). I defined a lot of the objects´ attributes with the keyword let, like the tomatos position with tomatoX and tomatoY, the tomatos speed and size, and the cars speed, width and height. I also used methods like move(), display(), and hits() where I defined the cars´ behaviour, such as moving them horizontally and checking for collisions with the tomato. 

**Draw upon the assigned reading, what are the characteristics of object-oriented programming and the wider implications of abstraction?**

The characteristics of object-oriented programming is different objects that communicate together in a program, for example the tomato and the cars in my game. These objects have propertires that control their attributes and looks, and methods that control their behaviour. We can use "classes" as an overview of the objects properties and behaviours. 


**Connect your game project to a wider cultural context, and think of an example to describe how complex details and operations are being “abstracted”?**

The program simplifies the complexities of real-life traffic rules used when crossing a road into an intuitive game, making the game accessible and enjoyable for a wide audience. Players don't need to worry about the detailed mechanics of traffic flow or road infrastructure, they just focus on timing their movements to avoid collisions with the cars.

**References**

https://p5js.org/reference/#/p5.Element/class

https://p5js.org/reference/#/p5/noLoop

Soon Winnie & Cox Geoff (2020), "Object Abstraction", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press (Chapter 6)