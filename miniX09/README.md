## MiniX09

![](Flowchart_minix4.png)

**What are the difficulties involved in trying to keep things simple at the
communications level whilst maintaining complexity at the algorithmic
procedural level?**

It can be challenging to maintain the complexity of a program, whilst also trying to communicate it in an understandable way. Simplifying too much may result in ambiguity, while providing too much detail may overwhelm the audience. It can be helpful getting to know your audiences' knowledge and understandig of the subject beforehand, so you know at which level you can express the programs functions and procedures. 

**What are the technical challenges facing the two ideas and how are you going to
address these?**

In the group section of this miniX, we were supposed to make flowcharts for two ideas for out final project. There might be some technical challenges in this process, because we have not actually tested the procedures of the ideas. Therefore, the ideas might not be realistic regarding out knowledge and skills when it comes to programming, and we might have to make a few changes when we actually start coding. 

**In which ways are the individual and the group flowcharts you produced useful?**

I think the individual flowchart I made of my miniX04 was helpful, because it made me revisit my though-process when I first made it, and further reflect over my design decisions. The group flowcharts we made was helpful to make sure the whole group had the same vision of the ideas, and it was easier to explain our thoughts and ideas to the other group members. 