let assumptions = ["YOU'RE GREEDY!", "YOU'RE LAZY!", "YOU'RE UNHEALTHY!", "YOU'RE HORNY!"];
let money, bed, burger, sex;
let imgWidth, imgHeight;
let capture;  // Variable to hold the camera feed

// Load the images
function preload() {  
  money = loadImage('image1.jpg')
  bed = loadImage('image2.jpg')
  burger = loadImage('image3.jpg')
  sex = loadImage('image4.jpg')
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  imgWidth = width / 2;
  imgHeight = height / 2;

  capture = createCapture(VIDEO);
  capture.size(imgWidth, imgHeight);
  capture.hide();
  currentImage = null; 
}

function draw() {
  background(255);

  image(money, 0, 0, imgWidth, imgHeight);
  image(bed, width/2, 0, imgWidth, imgHeight);
  image(burger, 0, height/2, imgWidth, imgHeight);
  image(sex, width/2, height/2, imgWidth, imgHeight);

 // Check if mouse is over an image and display video and text
 if (mouseX < imgWidth && mouseY < imgHeight) {  // If the mouse is over the money image
  displayVideoFeed(0, 0);
  displayAssumptionText(assumptions[0], imgWidth / 2, imgHeight / 2);
} else if (mouseX > width/2 && mouseY < imgHeight) {  // If the mouse is over the bed image
  displayVideoFeed(width/2, 0);
  displayAssumptionText(assumptions[1], width / 2 + imgWidth / 2, imgHeight / 2);
} else if (mouseX < imgWidth && mouseY > height/2) {  // If the mouse is over the burger image
  displayVideoFeed(0, height/2);
  displayAssumptionText(assumptions[2], imgWidth / 2, height / 2 + imgHeight / 2);
} else if (mouseX > width/2 && mouseY > height/2) {  // If the mouse is over the sex image
  displayVideoFeed(width/2, height/2);
  displayAssumptionText(assumptions[3], width / 2 + imgWidth / 2, height / 2 + imgHeight / 2);
}
}

// Function to display webcam feed
function displayVideoFeed(x, y) { 
image(capture, x, y, imgWidth, imgHeight);
}

// Function to display assumption text
function displayAssumptionText(displayText, x, y) {
fill(0);
textSize(60);
textAlign(CENTER, CENTER);
text(displayText, x, y); // Display assumption text at the center of the current image
}