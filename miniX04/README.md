## MiniX04

[Click to view my **miniX04**](https://tilla-ap.gitlab.io/aesthetic-programming/miniX04/index.html)

[Click to view my **code**](https://gitlab.com/tilla-ap/aesthetic-programming/-/blob/main/miniX04/sketch.js)

![](miniX04Video.mov)

**Provide a title for and a short description of your work as if you were going to submit it to the Transmediale festival.**

*Assumptions 2024*

In todays digital age, our machines collect huge amounts of data from various sources and domains. To make sense of this data we rely on assumptions to simplify and define what this data means, and turn it into knowledge and information. Assumptions influence the interpretation of results, but they also help to select appropriate methods and algorithms. 

The assumptions behind data capturing inspired me to try and showcase them in a fun and simplified way. I tried illustrating how computers think they know us based on every little move we do online. Cookies, algorithms and your personal choies all contribute to create a digital version of you, that programs can use to get to know you and your preferences. In one way, this may save you time and make your online experience better, because the content you are presented fits your interest. On the other hand, it can also make you even more addicted and make you buy or do things you normally wouldn´t have, just because it landed right in front of your face.

**Describe your program and what you have used and learnt.**

As you move the cursor around on the page, an assumption appears according to the image you are hovering over. The images represent common human temptations, like money, food, sex and relaxation. The assumption is related to the temptation on the image, therefore making an assumption about the user based on tracking of the mousepad. In addition to the assumption, the image also changes to a live image of yourself. It was a function a chose to add to emphasize that the assumption is about you, and puts the user in a more vulnerable position. 

I therefore used both mouse capture and video capture in my program. To capture the mouse movements I used if and else-if statements to define where the mouse is, and what will happen with each of the 4 images when the mouse is within its frame. Then, I created two functions, one that controls the assumption text, and one that controls the video image. 

**Articulate how your program and thinking address the theme of “capture all.”**

My program adresses the theme of "capture all" by giving you the impression that data is collected by where on the page the user moves the mouse. The program does not actually save any information of the users interactions, but the communication with the user through both words and picture, is supposed to make the user reflect on this specific topic. 

**What are the cultural implications of data capture?**

Cultural implications refer to affects and consequences data capture might have on our society, with the most obvious concern being privacy. Capturing data without consent or saving personal information like name, adress, email, browser habits etc. without disclosing its purpose of use, can be seen as an invasion of privacy. Another cultural concern might be discrimination. If a program is saving information about race, gender, religion, or sexual orientation, if can affect which ads or what content appears in their feed, and this may lead to certain groups being excluded or missing oppertunites. It is therefore important to protect the privacy of users, and to prevent discrimination when collecting data. 

**References**

https://p5js.org/reference/#/p5/createCapture

https://suneelpatel18.medium.com/assumptions-in-data-science-the-key-to-accurate-analysis-and-better-business-outcomes-47aba4e0ac3c

https://www.linkedin.com/pulse/ethics-data-collection-explore-ethical-implications-collecting

Soon Winnie & Cox Geoff (2020), "Data Capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, pp. 97-120 (Chapter 4)