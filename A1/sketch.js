// Define the Bird class to represent Pepita
class Bird {
    constructor() {
    this.energy = 0;  
    this.updateEnergy();  
  }
    fly(kilometers){
    this.energy = this.energy - (40 + kilometers * 2)
    this.updateEnergy();
  }
    eat(grams){ 
    this.energy = this.energy + (grams * 4)
    this.updateEnergy();
  }
    updateEnergy() {
    document.getElementById('energyDisplay').textContent = this.energy;
  }
}

// Create a new Bird instance named pepita
let pepita = new Bird()

// Event listener for the "Fly" button
document.getElementById('flyButton').addEventListener('click', () => {
    const kilometers = parseInt(document.getElementById('flyInput').value, 10);  // Get the number of kilometers from the input field and convert it to an integer
    pepita.fly(kilometers);
});

// Event listener for the "Eat" button
document.getElementById('eatButton').addEventListener('click', () => {
    const grams = parseInt(document.getElementById('eatInput').value, 10); // Get the number of grams from the input field and convert it to an integer
    pepita.eat(grams);
});