// Defining variables
let words = ["wait", "2 sec", "not yet", "hang on", "stand by", "sit tight", 
"soon", "almost", "not quite"];  //an array of words for the throbber
let currentIndex = 0;  //used to keep track of which word to highlight
let throbberFrame = 0;  //used to keep track of how many frames have passed
let throbberSpeed = 2;  //the pace of the throbber
let wordOpacity = [];  //to make the words appear and fade
let num = words.length;  //the number of words
let centerX, centerY, radius; //to center the throbber
let textTimer = 0;  //timer for the BE PATIENT text

function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(30);  //set the pace
  centerX = width / 2;
  centerY = height / 2;
  radius = 90;

  for (let i = 0; i < num; i++) {
    wordOpacity.push(0);  //the loop will continue as long as the number of words is more than 0, and for each iteration it adds +1
  }
}

function draw() {
  background(10, 80);  //black + opacity
  drawElements();  //calls the drawElements funtion
  drawText();  //calls the drawText function
  if (textTimer > 0){  //checks if textTimer is greater than 0, controlling the visibility of the "BE PATIENT" text
    textTimer--;  //decreases textTimer by 1 on each frame, making the "BE PATIENT" text fade out when it reaches 0
  }
}

function drawElements() {
  let wordSpacing = TWO_PI / num;  //spaces the words evenly across a circle
  for (let i = 0; i < num; i++) {  //starts a loop that repeats as many times as number of words
    let angle = i * wordSpacing - HALF_PI;  //calculate the angle for each word to position them evenly around the circle
    let x = centerX + cos(angle) * radius;  //calculates the x-coordinate of the current word
    let y = centerY + sin(angle) * radius;  //calculates the y-coordinate of the currrent word
    textAlign(CENTER, CENTER);   //centers the words within the circle
    fill(255);  //white text
  
    if (i === currentIndex) {  //if i is equal to currentIndex, we are drawing a currently highlighted word 
      let alpha = map(sin(throbberFrame * 0.1), -1, 1, 100, 255);  //calculates the transpary value of the text
      fill(255, alpha);  //white text with the opacity value
      text(words[i], x, y);  //draws the current word at the specified x- and y-coordinates
    } else {  //if i is not equal to the currentIndex, we are drawing a non-highlighted word
      fill(255, wordOpacity[i]);  //the opacity value of 0 from the wordOpacity array
      text(words[i], x, y);  //draws the current word at the specified x- and y-coordinates
    }
  }
  throbberFrame++;
  if (throbberFrame >= throbberSpeed) {  //checks if its time to change the highlighted word according to throbbeInterval
    throbberFrame = 0;  //resets the value back to 0 
    currentIndex = (currentIndex + 1) % num;  //moves on to the next word in the array and starts again when it reaches the end
    resetWordOpacity();  //resets the opacity for all word exepct the current one
  } else {
    wordOpacity[currentIndex] = min(wordOpacity[currentIndex] + 20, 255);  //gradually increases the opacity of the current word
  }
}

function resetWordOpacity() {
  for (let i = 0; i < num; i++) { //makes the loop iterate through all the words in the array
    if (i !== currentIndex) {  //if the word is not equal to currentIndex, it means it´s not currently highlighted
      wordOpacity[i] = 0;  //if it is not currently highlighted it should be fully transparent 
    }
  }
}

function drawText(){
  if (throbberSpeed === 12 && textTimer > 0) {  //when the mouse is pressed, the BE PATIENT text will appear
    push();
    fill(255);  //white text
    textSize(230);
    textAlign(CENTER, CENTER);
    text("BE PATIENT.", width / 2, height / 2);
    pop();
  }
}

function mousePressed() {
  if (throbberSpeed === 2) {  //when mouse is pressed, the throbber will slow down and text will appear
    throbberSpeed = 12; 
    textTimer = 20; 
    drawText();
  } else {
    throbberSpeed = 2; //if the thobber speed is not equal to 2 when mouse is pressed, the throbbber will then set to 2
  }
}