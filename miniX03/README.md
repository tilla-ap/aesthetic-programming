## MiniX03

[_Click to view my **miniX03**_](https://tilla-ap.gitlab.io/aesthetic-programming/miniX03/throbber.html)

[_Click to view my **code**_](https://gitlab.com/tilla-ap/aesthetic-programming/-/blob/main/miniX03/sketch.js)

![](eksempel.video.mov)

**What do you want to explore and/or express?**

With this exercise I wanted to explore for-loops, and get a better understanding of if-else statements. The thobber I ended up making is trying to express the frustrastion behind waiting while something is loading. In one way, a throbber is telling one to wait, so therefore I changed the standard circles of a throbber to words associated with waiting. And to make the throbber even more provocative, I added a mousePressed function to make the throbber go slower. So if you were to get tired of waiting and click on the page in frustration, the words "BE PATIENT" appears on the screen, and the throbber slows down. 

**What are the time-related syntaxes/functions that you have used in your
program, and why have you used them in this way? How is time being
constructed in computation (refer to both the reading materials and
your coding)?**

I have used several time-related functions in my program:
- frameRate: controls how many frames that shows each second, which effects the pace of the animation
- throbberFrame: keeps track of how many frames have passed since the last update of the animation, ensuring that the throbber starts over again smoothly 
- throbberSpeed: determines the pace of the throbber (how many frames should pass before switching to the next word)
- textTimer: a set variable to control how long the BE PATIENT text should appear on the screen (the timer decrementes from 20 until it reaches 0, at which point the text is no longer displayed)

Like Lammerant (2018) described in his chapter on how humans and machines experience time, our experience of time is strongly influenced by design decisions and implementations. I therefore chose to cleary express the waiting process of a throbber that other throbbers try to hide with their design.

**Think about a throbber that you have encounted in digital culture, e.g. for
streaming video on YouTube or loading the latest feeds on Facebook, or waiting
for a payment transaction, and consider what a throbber communicates, and/or
hides? How might we characterize this icon differently?**

A throbber shows that an operation is in progess, but it does not show what and how it´s done. The feelings I get while waiting on throbber varies according to what operation I am waiting on, and how the throbber is designed. For example, the throbber used in my fitness center´s app is a flexed arm. I  think this is a cool idea that fits the purpose of the app, and since I am only using the app to book a workout class, I don´t really mind the few seconds I need to wait to be redirected to the next page. On the other hand, if I am buying concert tickets where you are in a hurry to secure a ticket with your payment, the basic, green, circular thobber makes me very stressed and it feels like you are waiting forever on it to finish loading. 

**References**

https://p5js.org/reference/#/p5/for

https://p5js.org/reference/#/p5/textAlign

https://p5js.org/reference/#/p5/if-else 

Soon Winnie & Cox Geoff (2020), "Infinite Loops", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, p. 71-96 (chapter 3)

Hans Lammerant (2018), “How humans and machines negotiate experience of time,” in The Techno-Galactic Guide to Software Observation, p. 88-98