function setup() {
createCanvas(600,600);
}

function draw() {
  background(255); //white
  
  //Left sock
  fill(100, 200, 60); //green sock
  noStroke();
  push(); //save the current settings
  translate(10, 20); // move the left sock further down
  rect(30, 20, 50, 100, 10);
  push(); //save the current setting
  translate(148, 160);
  rotate(PI / 4);
  ellipse(-80, 30, 100, 50);
  pop(); //restore the settings

  fill(255, 200, 0); // yellow stripes
  rect(30, 30, 50, 10); 
  rect(30, 50, 50, 10); 

  //Right sock
  pop(); //restore the settings
  fill(250, 100, 60);
  rect(100, 20, 50, 100, 10);
  translate(148, 160);
  rotate(PI / 4);
  ellipse(-30, -20, 100, 50);

  fill(0, 150, 250); //blue stripes
  translate(-140, -185);
  rotate(PI / 4);
  rect(100, 30, 10, 50); 
  rect(120, 30, 10, 50); 

}
