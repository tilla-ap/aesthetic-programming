## MiniX02

[_Click to view my **miniX2: rock you socks emoji**_](https://tilla-ap.gitlab.io/aesthetic-programming/miniX02/rockesokk.html)

[_Click to view my **code**_](https://gitlab.com/tilla-ap/aesthetic-programming/-/blob/main/miniX02/sketch1.js?ref_type=heads)

[_Click to view my **miniX2: women´s health emoji**_](https://tilla-ap.gitlab.io/aesthetic-programming/miniX02/kvinnehelse.html)

[_Click to view my **code**_](https://gitlab.com/tilla-ap/aesthetic-programming/-/blob/main/miniX02/sketch2.js?ref_type=heads)

**Describe your program and what you have used and learnt.**

When designing my emojis, I explored and played with shapes, colors, sizes and angles in p5. I learnt 4 new functions with this exercise:
- rotate(): rotates shapes and objects
- translate(): helps you easily move object and shapes around on the screen
- push(): saves the current drawing style settings and transformations
- pop(): restores the settings saved with push

**How would you put your emojis into a wider social and cultural context that
concerns a politics of representation, identity, race, colonialism, and so on?**

Going into this assignment, I had a hard time figuring out what emojis I wanted to design. Among the thousands of emojis already excisting, is there something I am missing?

After some thought, I found out I wanted to honor my uncle who recently passed, by designing an emoji representing people with Downs syndrom. It is this Abbing, Pierrot and Snelting talks about in their chapter "Modifying the Universal". Emojis have made us rethink what it means to say "everyone", and they have gone from just a form of expression to a form of identification. These identifications are being used in political discourse, cultural representation, and social activism in our digital age, and have altered the landscape of digital communication, influencing the way people express themselves online. I therefore designed an emoji of a pair of two different colorful socks, that represents the rock your socks campaign that marks the World Downs Syndrom Day on March 21st. The campaign is a fun way to celebrate our differences, and raise awareness and show support for the rights and inclusion of people with Downs syndrom. 

The other emoji I designed was one to represent womens´s health. I decided to combine two symbols with this emoji, the female sex symbol and the red cross health care symbol. The feminist movement has come a long way in recent years, but still we know hardly enough about the female body, when most of the medicial knowledge is based on the male body. Emojis to play an important role in social activism online, as we have seen with the Black Lives movement and the gay rights movement. Just like the rock your socks emoji, this emoji could contribute in raising awareness on an important topic in the digital landscape. 

**References**

https://p5js.org/reference/#/p5/rotate

https://p5js.org/reference/#/p5/translate

https://p5js.org/reference/#/p5/push

https://p5js.org/reference/#/p5/pop

Soon Winnie & Cox, Geoff, "Variable Geometry", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 51-70 (chapter 2)

Roel Roscam Abbing, Peggy Pierrot and Femke Snelting (2018), "Modifying the Universal." Executing Practices, Eds. Helen Pritchard, Eric Snodgrass & Magda Tyżlik-Carver, London: Open Humanities Press, p. 35-51
