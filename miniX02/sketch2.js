function setup() {
createCanvas(1500,900)
}

function draw() {
  background(244, 194, 194) //babypink 
  
  //female gender symbol
  noStroke()
  fill(255) //white
  ellipse(140,120,140)
  rect(125,180,30,80)
  rect(105,209,70,27)

  //red cross health care symbol
  fill(200,0,0) //red
  rect(127,82,25,75)
  rect(103,108,75,25)
}
